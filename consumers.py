import json
import pika
import django
from sys import path
import os

path.append(
    '/sample_django/settings.py')  # Your path to settings.py file
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sample_django.settings')
django.setup()
from app.models import Employee

connection = pika.BlockingConnection(
    pika.ConnectionParameters('localhost', heartbeat=600,
                              blocked_connection_timeout=300))
channel = connection.channel()
channel.queue_declare(queue='app')


def callback(ch, method, properties, body):
    print("Received in details...")
    print(body)
    data = json.loads(body)
    print(data)

    if properties.content_type == 'quote_created':
        quote = Employee.objects.create(id=data['id'], title=data['title'])
        quote.save()
        print("Employee created")
    elif properties.content_type == 'quote_updated':
        quote = Employee.objects.get(id=data['id'])
        quote.title = data['title']
        quote.save()
        print("quote updated")
    elif properties.content_type == 'quote_deleted':
        quote = Employee.objects.get(id=data)
        quote.delete()
        print("quote deleted")


channel.basic_consume(queue='app', on_message_callback=callback,
                      auto_ack=True)
print("Started Consuming...")
channel.start_consuming()
