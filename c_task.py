import django
from celery import Celery, shared_task
import time
from celery import app as celery_app
import os
from celery import Celery
import time

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sample_django.settings')
django.setup()
from app.models import *
app = Celery('tasks', broker='pyamqp://guest@localhost//')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

__all__ = ['celery_app']


@app.task
def add(a, b):
    print("induja")
    time.sleep(5)
    print('induja 2')
    time.sleep(5)
    print('induja 3')
    time.sleep(5)
    print('induja 4')
    time.sleep(5)
    print('induja 5')
    time.sleep(5)
    print('induja 6')
    time.sleep(5)
    print('induja 7')
    time.sleep(5)
    print('induja 8')

    return a + b


@app.task
def sample():
    is_active = False
    employees = Employee.objects.all()
    print(employees)
    result = []
    for employee in employees:
        if not employee.is_active:
            result.append(employee.id)

    time.sleep(5)
    print('current status is not active')
    print(F'result: {result}')



