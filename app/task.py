from celery import Celery
import time


app = Celery('tasks', broker='pyamqp://guest@localhost//')


@app.task
def add(a, b):
    print("induja")
    time.sleep(5)
    print('induja 2')
    time.sleep(5)
    print('induja 3')
    time.sleep(5)
    print('induja 4')
    time.sleep(5)
    print('induja 5')
    time.sleep(5)
    print('induja 6')
    time.sleep(5)
    print('induja 7')
    time.sleep(5)
    print('induja 8')

    return a + b


@app.task
def sample(is_active):
    if is_active == 'on':
        is_active = True
    else:
        is_active = False
    if not is_active:
        time.sleep(5)
        print('current status is not active')
    else:
        print('current status is active')




