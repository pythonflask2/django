from .models import Employee, Address, Login, Person, Location
from rest_framework import serializers


class Employee_serializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = '__all__'
        extra_kwargs = {'id': {'required': False}}


class Address_serializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = '__all__'
        extra_kwargs = {"location": {"read_only": True}}


class Login_serializer(serializers.ModelSerializer):
    class Meta:
        model = Login
        fields = [
            'id',
            'user_name',
            'password',
            'admin',
        ]


class Person_serializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = '__all__'


class Location_serializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ' __all__'
        extra_kwargs = {"location": {"read_only": True}}
