from django.contrib.auth.hashers import make_password
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.generics import CreateAPIView, RetrieveAPIView, \
    DestroyAPIView, ListAPIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from . import YourException
from .models import Employee

from send import publish
from .serializer import Login_serializer, Employee_serializer
import logging
from django.conf import settings
from django.shortcuts import render

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
tracing = settings.OPENTRACING_TRACING


@api_view(['POST'])
def post(request):
    user = request.data
    serializer = Login_serializer(data=user, context={'request': request})
    if serializer.is_valid():
        user_saved = serializer.save(
            password=make_password(user['password']))
        return Response(user_saved,
                        status=200)
    return Response({
        "error": "Error encountered"},
        status=406)


class EmployeeAPIVIEW(CreateAPIView):
    serializer_class = Employee_serializer
    queryset = Employee.objects.all()

    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        try:
            employee = Employee()
            employee.employee_name = request.data['employee_name']
            employee.admin = request.data['admin']
            employee.date_of_join = request.data['date_of_join']
            employee.role = request.data['role']
            employee.contact_number = request.data['contact_number']
            employee.is_active = request.data['is_active']
            employee.save()
            logger.info(f'Employee Details inserted at{employee.id}')
            publish('Employee_created', 'Employee_created')
            return Response({"status": 'Employee details added}'},
                            status=status.HTTP_201_CREATED)
        except:
            raise YourException('Invalid Details')


class GETAPIVIEW(ListAPIView):
    serializer_class = Employee_serializer
    queryset = Employee.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            employee = Employee.objects.all()
            print(employee)

            result = []

            for employee_details in employee:
                employee_data = {}
                employee_data['admin'] = employee_details.admin
                employee_data[
                    'employee_name'] = employee_details.employee_name
                employee_data['role'] = employee_details.role
                employee_data[
                    'date_of_join'] = employee_details.date_of_join
                employee_data[
                    'contact_number'] = employee_details.contact_number
                employee_data['is_active'] = employee_details.is_active

                result.append(employee_data)
                logger.info(f'Returns all the Employee details')
                publish('All Employee details', 'All Employee details')
            return Response({'employee_details': result},
                            status=status.HTTP_200_OK)
        except:
            raise YourException(
                'There is No Details to Return employee details')


class GETSINGLEAPIVIEW(RetrieveAPIView):
    serializer_class = Employee_serializer
    queryset = Employee.objects.all()

    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            id = kwargs.get('pk')
            employee = Employee.objects.filter(id=id).all()
            result = []

            for employee_details in employee:
                employee_data = {}
                employee_data['admin'] = employee_details.admin
                employee_data[
                    'employee_name'] = employee_details.employee_name
                employee_data['role'] = employee_details.role
                employee_data[
                    'date_of_join'] = employee_details.date_of_join
                employee_data[
                    'contact_number'] = employee_details.contact_number

                result.append(employee_data)
                logger.info(f'Returns the Employee details{id}')
                publish('Employee details', 'employee_details received')
            return Response({'employee_details': result},
                            status=status.HTTP_200_OK)
        except:
            raise YourException(f'The Given ID is invalid')


class DELETEAPIVIEW(DestroyAPIView):
    serializer_class = Employee_serializer
    queryset = Employee.objects.all()

    # permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        try:
            id = kwargs.get('pk')
            user = Employee.objects.filter(id=id)
            user.delete()
            logger.info(f'Delete the employee Details using {id}')
            publish('Employee_details deleted', 'Employee_details deleted')
            return Response({'message': "Employee details deleted"})
        except:
            raise YourException('There No such a Id to delete')


class PaginatedListView(ListAPIView):
    queryset = Employee.objects.all()
    serializer_class = Employee_serializer
    paginate_by = 10
    paginate_by_param = 'page_size'
    max_paginate_by = 100


def index(request):
    return render(request, 'index.html')


