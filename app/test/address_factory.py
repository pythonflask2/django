import factory

from app.test.factory import UserFactory


class UserFactoryAddress(factory.django.DjangoModelFactory):
    id = factory.Faker.override_default_locale('id')
    place_of_birth = factory.Faker.override_default_locale('place_of_birth')
    city = factory.Faker.override_default_locale('city')
    nationality = factory.Faker.override_default_locale('nationality')
    employee_id = factory.SubFactory(UserFactory)

    class Meta:
        model = 'app.Address'
