from django.forms import model_to_dict
from rest_framework.test import APIClient, APITestCase
from django.urls import reverse
from rest_framework import status
from faker import Faker
from nose.tools import eq_
from app.test.factory import UserFactory
import os

fake = Faker()


class TestMongoEmployee(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        cls.employee_details = model_to_dict(UserFactory.build())
        cls.employee_name = 'string'
        cls.post_url = reverse('mongo_employee_insert')
        cls.detail_url = reverse('mongo_employee_get_name',
                                 kwargs={'employee_name': cls.employee_name})
        cls.list_url = reverse('mongo_employee_get')
        cls.neg_url = reverse('mongo_employee_delete',
                              kwargs={'employee_name': cls.employee_name})

    def test_mongo_post_employee(self):
        data = {
            "employee_name": "test",
            "role": "developer",
            "contact_number": "9087654321",
            "date_of_join": "2022-06-21 07:15:41.021000",
            "is_active": True,
            "admin": True,
        }
        response = self.client.post(self.post_url, data=data, format='json')
        eq_(response.status_code, status.HTTP_201_CREATED)

    def test_mongo_get_by_id_employee(self):
        response = self.client.get(self.detail_url)
        eq_(response.status_code, status.HTTP_200_OK)

    def test_mongo_get(self):
        response = self.client.get(self.list_url)
        eq_(response.status_code, status.HTTP_200_OK)

    # def test_mongo_delete(self):
    #     response = self.client.get(self.neg_url)
    #     eq_(response.status_code, status.HTTP_200_OK)
