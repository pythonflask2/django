from django.urls import reverse
from faker import Faker
from rest_framework import status
from nose.tools import eq_
from app.test.person_factory import UserFactoryPerson
import os
from django.forms import model_to_dict
from rest_framework.test import APIClient, APITestCase

fake = Faker()
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sample_django.settings')


class TestMongoPerson(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        cls.person_details = model_to_dict(UserFactoryPerson.build())
        cls.person_name = 'string'
        cls.post_url = reverse('mongo_person_insert')
        cls.detail_url = reverse('mongo_person_get_id',
                                 kwargs={'person_name': cls.person_name})
        cls.list_url = reverse('mongo_person_get')
        cls.neg_url = reverse('mongo_person_delete',
                              kwargs={'person_name': cls.person_name})

    def test_post_mongo_person(self):
        data = {
            "person_name": "abitha",
            "age": "21",
            "dob": "2000-06-15T13:45:30",
            "employee_id": 1,
            "admin": True
        }
        response = self.client.post(self.post_url, data=data, format='json')
        eq_(response.status_code, status.HTTP_201_CREATED)

    def test_get_mongo_person(self):
        response = self.client.get(self.detail_url)
        eq_(response.status_code, status.HTTP_200_OK)

    def test_get_all_person(self):
        response = self.client.get(self.list_url)
        eq_(response.status_code, status.HTTP_200_OK)

