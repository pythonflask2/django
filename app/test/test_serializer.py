from django.test import TestCase
from django.forms.models import model_to_dict
from app.serializer import Employee_serializer
from app.test.factory import UserFactory
from nose.tools import eq_


class TestCreateUserSerializer(TestCase):

    def setUp(self):
        user_data = model_to_dict(UserFactory.build())
        self.user_data = user_data

    def test_serializer_with_empty_data(self):
        serializer = Employee_serializer(data={})
        eq_(serializer.is_valid(), False)
