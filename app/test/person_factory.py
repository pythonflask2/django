import factory

from app.test.factory import UserFactory


class UserFactoryPerson(factory.django.DjangoModelFactory):
    id = factory.Faker.override_default_locale('id')
    person_name = factory.Faker.override_default_locale('person_name')
    dob = factory.Faker.override_default_locale('dob')
    age = factory.Faker.override_default_locale('age')
    employee_id = factory.SubFactory(UserFactory)

    class Meta:
        model = 'app.Person'
