from django.urls import reverse
from faker import Faker
from rest_framework import status
from nose.tools import eq_
from app.test.address_factory import UserFactoryAddress
import os
from django.forms import model_to_dict
from rest_framework.test import APIClient, APITestCase

fake = Faker()
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sample_django.settings')


class TestMongoAddress(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        cls.address_details = model_to_dict(UserFactoryAddress.build())
        cls.city = 'string'
        cls.post_url = reverse('mongo_address_insert')
        # cls.detail_url = reverse('mongo_address_get_id', kwargs={'city': cls.city})
        cls.list_url = reverse('mongo_address_get')
        cls.neg_url = reverse('mongo_address_delete', kwargs={'city': cls.city})

    def test_post_mongo_address(self):
        data = {
            "city": "salem",
            "nationality": "Indian",
            "place_of_birth": "Namakkal",
            "employee_id": 1
        }
        response = self.client.post(self.post_url, data=data)
        eq_(response.status_code, status.HTTP_201_CREATED)

    def test_get_address_byname(self):
        response = self.client.get(self.list_url)
        eq_(response.status_code, status.HTTP_200_OK)

    # def test_get_address_mongo(self):
    #     response = self.client.get(self.detail_url)
    #     eq_(response.status_code, status.HTTP_200_OK)

    # def test_delete_address_mongo(self):
    #     response = self.client.get(self.neg_url)
    #     eq_(response.status_code, status.HTTP_200_OK)
