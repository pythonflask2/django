from django.urls import reverse
from faker import Faker
from rest_framework import status
from nose.tools import eq_
from app.test.person_factory import UserFactoryPerson
import os
from django.forms import model_to_dict
from rest_framework.test import APIClient, APITestCase

fake = Faker()
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sample_django.settings')


class TestPerson(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        cls.person_details = model_to_dict(UserFactoryPerson.build())
        # cls.employee = UserFactory.create()
        cls.pk = 1
        cls.detail_url = reverse('get_person_details', kwargs={'pk': cls.pk})
        cls.neg_url = reverse('delete_person',
                              kwargs={'pk': cls.pk})
        cls.list_url = reverse('get_all_person_details')
        cls.post_url = reverse('person')

    def test_post_address(self):
        data = {
            "person_name": "abitha",
            "age": "21",
            "dob": "2000-06-15T13:45:30",
            "employee_id": 1,
            "admin": True
        }
        response = self.client.post(self.post_url, data=data, format='json')
        self._test_get_all_address()
        self._test_get_address_by_id()
        self._test_delete_address()
        eq_(response.status_code, status.HTTP_201_CREATED)

    def _test_get_address_by_id(self):
        response = self.client.get(self.detail_url)
        eq_(response.status_code, status.HTTP_200_OK)

    def _test_get_all_address(self):
        response = self.client.get(self.list_url)
        eq_(response.status_code, status.HTTP_200_OK)

    def _test_delete_address(self):
        response = self.client.delete(self.neg_url)
        eq_(response.status_code, status.HTTP_200_OK)

    def _test_all_address(self):
        pass
