from django.urls import reverse
from faker import Faker
from rest_framework import status
from nose.tools import eq_
from app.test.address_factory import UserFactoryAddress
import os
from django.forms import model_to_dict
from rest_framework.test import APIClient, APITestCase

fake = Faker()
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sample_django.settings')


class TestAddress(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = APIClient()
        cls.address_details = model_to_dict(UserFactoryAddress.build())
        # cls.employee = UserFactory.create()
        cls.pk = 1
        cls.detail_url = reverse('get_address', kwargs={'pk': cls.pk})
        cls.neg_url = reverse('delete_address',
                              kwargs={'pk': cls.pk})
        cls.list_url = reverse('get_address_details')
        cls.post_url = reverse('add')

    def test_post_address(self):
        data = {
            "city": "salem",
            "nationality": "Indian",
            "place_of_birth": "Namakkal",
            "employee_id": 1
        }
        response = self.client.post(self.post_url, data=data)
        self._test_get_all_address()
        self._test_get_address_by_id()
        self._test_delete_address()
        eq_(response.status_code, status.HTTP_201_CREATED)

    def _test_get_address_by_id(self):
        response = self.client.get(self.detail_url)
        eq_(response.status_code, status.HTTP_200_OK)

    def _test_get_all_address(self):
        response = self.client.get(self.list_url)
        eq_(response.status_code, status.HTTP_200_OK)

    def _test_delete_address(self):
        response = self.client.delete(self.neg_url)
        eq_(response.status_code, status.HTTP_200_OK)
