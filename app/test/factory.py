import factory


class UserFactory(factory.django.DjangoModelFactory):
    id = factory.Faker.override_default_locale('id')
    employee_name = factory.Faker.override_default_locale('employee_name')
    admin = True
    role = factory.Faker.override_default_locale('role')
    date_of_join = factory.Faker.override_default_locale('dae_of_join')
    contact_number = factory.Faker.override_default_locale('contact_number')
    is_active = True

    class Meta:
        model = 'app.Employee'
