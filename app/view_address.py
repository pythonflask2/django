from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from send import publish
from . import YourException
from .models import Address, Employee
from rest_framework.generics import CreateAPIView, RetrieveAPIView, \
    DestroyAPIView, ListAPIView

from .serializer import Address_serializer

import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class INSERTAPIVIEW(CreateAPIView):
    serializer_class = Address_serializer
    queryset = Address.objects.all()
    # permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        try:
            address = Address()
            address.city = request.data['city']
            address.place_of_birth = request.data['place_of_birth']
            address.nationality = request.data['nationality']
            employee_id = request.data['employee_id']
            print(address.employee_id)
            address.employee_id = Employee.objects.get(id=employee_id)
            address.save()
            logger.info('Address details inserted')
            publish('Address created', 'Address created')
            return Response({'Address_details': 'Inserted successfully'},
                            status=status.HTTP_201_CREATED)
        except:
            raise YourException('Invalid Details')


class GETADDRESSAPIVIEW(ListAPIView):
    serializer_class = Address_serializer
    queryset = Address.objects.all()
    # permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            address = Address.objects.all()
            result = []

            for address_details in address:
                address_data = {}
                address_data['city'] = address_details.city
                address_data[
                    'place_of_birth'] = address_details.place_of_birth
                address_data['nationality'] = address_details.nationality
                address_data['employee_id'] = address_details.employee_id
                result.append(address_data)
                logger.info('Return all the address details')
                publish('All the Address details', 'All address details')
            return Response({'Address_details': result},
                            status=status.HTTP_200_OK)
        except:
            raise YourException('There is no details to Return')


class GETBYIDAPIVIEW(RetrieveAPIView):
    serializer_class = Address_serializer
    queryset = Address.objects.all()
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        try:
            id = kwargs.get('id')
            employee = Address.objects.filter(id=id).all()
            user_details = []
            for address_details in employee:
                address_data = {}
                address_data['city'] = address_details.city
                address_data[
                    'place_of_birth'] = address_details.place_of_birth
                address_data['nationality'] = address_details.nationality
                address_data['employee_id'] = address_details.employee_id
                user_details.append(address_data)
                logger.info(f'Address details for {id}')
                publish('Address details by Id', 'Address details by Id')
            return Response({'Address_details': user_details},
                            status=status.HTTP_200_OK)
        except:
            raise YourException('There Is No such a Id to get the Details')


class DELETEADDRESSAPIVIEW(DestroyAPIView):
    serializer_class = Address_serializer
    queryset = Address.objects.all()
    # permission_classes = (IsAuthenticated,)

    def delete(self, request, *args, **kwargs):
        try:
            id = kwargs.get('pk')
            instance = Address.objects.filter(id=id).first()
            instance.delete()
            logger.info(f'Deleted the address details: {id}')
            publish('Person deleted', 'person deleted')
            return Response({'message': "Address deleted"})
        except:
            raise YourException('There is No Such a Id to Delete')


class PaginatedListView(ListAPIView):
    queryset = Address.objects.all()
    serializer_class = Address_serializer
    paginate_by = 10
    paginate_by_param = 'page_size'
    max_paginate_by = 100

