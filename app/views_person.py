from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, RetrieveAPIView, \
    DestroyAPIView, ListAPIView

from send import publish
from .models import Person, Employee
from .serializer import Person_serializer
from . import YourException
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class INSERTPERSONAPIVIEW(CreateAPIView):
    serializer_class = Person_serializer
    queryset = Person.objects.all()

    def create(self, request, *args, **kwargs):
        try:
            person = Person()
            person.person_name = request.data['person_name']
            person.dob = request.data['dob']
            person.age = request.data['age']
            employee_id = request.data['employee_id']
            person.employee_id = Employee.objects.get(id=employee_id)
            person.save()
            logger.info('Person Details inserted')
            publish('Person created', 'Person_created')
            return Response({'person_details': 'Inserted successfully'},
                            status=status.HTTP_201_CREATED)
        except:
            raise YourException('Invaid Details to create a person details')


class GETALLAPIVIEW(ListAPIView):
    serializer_class = Person_serializer
    queryset = Person.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            person = Person.objects.all()
            result = []

            for person_details in person:
                person_data = {}
                person_data['person_name'] = person_details.person_name
                person_data[
                    'dob'] = person_details.dob
                person_data['age'] = person_details.age
                person_data['employee_id'] = person_details.employee_id
                result.append(person_data)
                logger.info('Return all the values of person')
                publish('All person details', 'All person details')
                return Response({'person_details': person_data},
                                status=status.HTTP_200_OK)
        except:
            raise YourException('There is no values to return the values')


class DLELETEAPIVIEW(DestroyAPIView):
    serializer_class = Person_serializer
    queryset = Person.objects.all()

    def delete(self, request, *args, **kwargs):
        try:
            id = kwargs.get('pk')
            user = Person.objects.filter(id=id)
            user.delete()
            logger.info(f'Preson details deleted {id}')
            publish('Person deleted', 'Person deleted')
            return Response({'message': "Person details deleted"})
        except:
            raise YourException('There is No such a Id to Delete the Details')


class GETBYPERSONIDAPIVIEW(RetrieveAPIView):
    serializer_class = Person_serializer
    queryset = Person.objects.all()

    def get(self, request, *args, **kwargs):
        try:
            id = kwargs.get('id')
            person = Person.objects.filter(id=id).all()
            user_details = []
            for person_details in person:
                person_data = {}
                person_data['person_name'] = person_details.person_name
                person_data[
                    'dob'] = person_details.dob
                person_data['age'] = person_details.age
                person_data['employee_id'] = person_details.employee_id
                # address_data['employee_id'] = address_details.employee_id
                user_details.append(person_data)
                logger.info('Return all the values of person by Id')
                publish('All the person details by Id ', 'All person details by Id')
            return Response({'person_details': user_details},
                            status=status.HTTP_200_OK)
        except:
            raise YourException('There is No such a Id to get the details')


class PaginatedListView(ListAPIView):
    queryset = Person.objects.all()
    serializer_class = Person_serializer
    paginate_by = 10
    paginate_by_param = 'page_size'
    max_paginate_by = 100
