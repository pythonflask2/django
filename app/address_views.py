import json

from bson import json_util
from rest_framework import status
from rest_framework.generics import CreateAPIView, RetrieveAPIView, \
    DestroyAPIView, ListAPIView
from rest_framework.response import Response

from app.models import Address
from app.mongo_views import collection2
from app.serializer import Address_serializer


class AddressAPIVIEW(CreateAPIView):
    serializer_class = Address_serializer
    queryset = Address.objects.all()

    def create(self, request, *args, **kwargs):
        city = request.data['city']
        place_of_birth = request.data['place_of_birth']
        nationality = request.data['nationality']
        employee_id = request.data['employee_id']
        document = {"place_of_birth": place_of_birth,
                    "city": city,
                    "nationality": nationality,
                    "employee_id": employee_id
                    }
        collection2.insert_one(document)
        return Response({f'Address_details inserted'},
                        status=status.HTTP_201_CREATED)


class GetAPIVIEW(ListAPIView):
    serializer_class = Address_serializer
    queryset = Address.objects.all()

    def get(self, request, *args, **kwargs):
        address_details = collection2.find()
        result = []
        for address in address_details:
            print(address)
            add = {}
            add['city'] = address.get('city')
            add['place_of_birth'] = address.get('place_of_birth')
            add['nationality'] = address.get('nationality')
            add['employee_id'] = address.get('employee_id')
            result.append(add)
            print(add)
        return Response(json.loads(json_util.dumps(result)))


class GetAPIVIEWS(RetrieveAPIView):
    serializer_class = Address_serializer
    queryset = Address.objects.all()

    def get(self, request, *args, **kwargs):
        employee_details = collection2.find_one({"city": "string"})
        print(employee_details)
        return Response(json.loads(json_util.dumps(employee_details)))


class DELETEAPIVIEW(DestroyAPIView):
    serializer_class = Address_serializer
    queryset = Address.objects.all()

    def delete(self, request, *args, **kwargs):
        city = kwargs.get('city')
        person_details = collection2.find_one_and_delete({'city': city})
        return Response({f"details deleted"})
