import json

import pymongo
from bson import json_util
from rest_framework import status
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView, RetrieveAPIView, \
    DestroyAPIView, ListAPIView

from app.models import Employee
from app.serializer import Employee_serializer

connection_url = 'mongodb://localhost:27017/'
databases = pymongo.MongoClient(connection_url)

database_name = 'employee_details'
database = databases[database_name]

collection_name = 'employee'
collection_name2 = 'address'
collection_name3 = 'person'
collection = database[collection_name]
collection2 = database[collection_name2]
collection3 = database[collection_name3]


class EmployeeAPIVIEW(CreateAPIView):
    serializer_class = Employee_serializer
    queryset = Employee.objects.all()

    def create(self, request, *args, **kwargs):
        employee_name = request.data['employee_name']
        admin = request.data['admin']
        date_of_join = request.data['date_of_join']
        role = request.data['role']
        contact_number = request.data['contact_number']
        document = {"employee_Name": employee_name,
                    "Role ": role,
                    "admin ": admin,
                    "contact_number": contact_number,
                    "date_of_join": date_of_join
                    }
        inserted_id = collection.insert_one(document).inserted_id
        return Response({f'employee_details inserted'},
                        status=status.HTTP_201_CREATED)


class GetAPIVIEW(ListAPIView):
    serializer_class = Employee_serializer
    queryset = Employee.objects.all()

    def get(self, request, *args, **kwargs):
        emp_details = collection.find()
        result = []
        for employee in emp_details:
            emp = {}
            emp['admin'] = employee.get('admin')
            emp['employee_name'] = employee.get('employee_name')
            emp['role'] = employee.get('role')
            emp['date_of_join'] = employee.get('date_of_join')
            emp['contact_number'] = employee.get('contact_number')
            result.append(emp)
        return Response(json.loads(json_util.dumps(result)))


class GetAPIVIEWS(RetrieveAPIView):
    serializer_class = Employee_serializer
    queryset = Employee.objects.all()

    def get(self, request, *args, **kwargs):
        employee_details = collection.find_one({"employee_Name": "string"})
        print(employee_details)
        return Response(json.loads(json_util.dumps(employee_details)))


class DELETEAPIVIEW(DestroyAPIView):
    serializer_class = Employee_serializer
    queryset = Employee.objects.all()

    def delete(self, request, *args, **kwargs):
        employee_name = kwargs.get('employee_name')
        person_details = collection.find_one_and_delete(
            {'employee_name': employee_name})
        return Response({f"person details deleted"})
