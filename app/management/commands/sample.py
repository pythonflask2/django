from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    info = 'something'

    def add_arguments(self, parser):
        parser.add_argument('something', nargs='+', type=str)

    def handle(self, *args, **options):
        for something in options['something']:
            try:
                if len(something) == 0:
                    raise Exception()
                print('success')
            except:
                print('Failure')
