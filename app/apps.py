from django.apps import AppConfig


class AppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app'




"""
curl -X POST "http://127.0.0.1:5001/app/employee/insert/" -H  "accept: application/json" -H  "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjU0Nzg2MjkyLCJpYXQiOjE2NTQ3ODYyMzIsImp0aSI6IjJkYWY1NzdkYmU0NjRlN2I4MTI0ZDU3MmJlOWY2N2I4IiwiaWQiOiJJc2h1In0.WdYzKgIG8ZUuHecsj9Q_CIonpPdThlFgwQtjHjul51s" -H  "Content-Type: application/json" -H  "X-CSRFToken: cUr0mCZ6R85nDKyC6gmPBpdBeV49e0JxLG2VLdjHgPs7VgLjN28uP3ldIE3R42ql" -d "{  \"employee_name\": \"string\",  \"role\": \"string\",  \"admin\": true,  \"date_of_join\": \"2022-06-09T14:41:32.901Z\",  \"contact_number\": \"string\"}"
"""