import json

import psutil
from asgiref.sync import async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.layers import get_channel_layer
from django.conf import settings


class SystemConsumer(AsyncWebsocketConsumer):
    group_name = 'sample123'

    async def connect(self):
        # Joining group
        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, close_code):
        # Leave group
        await self.channel_layer.group_discard(
            self.group_name,
            self.channel_name
        )

    async def receive(self, text_data):
        # Receive data from WebSocket
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        print(message)
        # Print message that receive from Websocket
        cpu_percent = psutil.cpu_percent()
        ram_percent = psutil.virtual_memory().percent
        await self.channel_layer.group_send(
            self.group_name,
            {
                'type': 'system_load',
                'data': {
                    'cpu_percent': cpu_percent,
                    # initial value for cpu and ram set to 0
                    'ram_percent': ram_percent
                }
            }
        )

    async def system_load(self, event):
        # Receive data from group
        await self.send(text_data=json.dumps(event['data']))



