from . import views, mongo_views
from django.urls import path
from drf_yasg.views import get_schema_view
from drf_yasg import openapi


schema_view = get_schema_view(
    openapi.Info(
        title="Matc API",
        default_version='v1',
        description="Test description",
    ),
    public=True

)

urlpatterns = [
    path('employee/insert/', views.EmployeeAPIVIEW.as_view(), name='insert'),
    path('employee/delete/<int:pk>', views.DELETEAPIVIEW.as_view(),
         name='delete'),
    path('employee/get_one/<int:pk>/', views.GETSINGLEAPIVIEW.as_view(),
         name='get_by_id'),
    path('employee/get/', views.GETAPIVIEW.as_view(), name='get_All'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0),
         name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0),
         name='schema-redoc'),
    path('app/page/', views.PaginatedListView.as_view(), name='pagination'),
    path('app/mongo/insert', mongo_views.EmployeeAPIVIEW.as_view(),
         name='mongo_employee_insert'),
    path('mongo/employee/get', mongo_views.GetAPIVIEW.as_view(),
         name='mongo_employee_get'),
    path('mongo/employee/getby_id/<str:employee_name>',
         mongo_views.GetAPIVIEWS.as_view(), name='mongo_employee_get_name'),
    path('mongo/employee/delete/<str:employee_name>',
         mongo_views.DELETEAPIVIEW.as_view(), name='mongo_employee_delete')
]

# urls = [
#     path("terms/<int:page>", views.listing, name="listing"),
#     path("employee.json", views.listing_api, name="terms-api")
# ]
