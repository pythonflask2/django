from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.contrib.gis.geoip2 import GeoIP2
geo_ip = GeoIP2()


@api_view(['GET'])
def geo_location(request):
    if request.method == "GET":
        try:
            geo = geo_ip.city(get_client_ip(request))
            json_data = {
                "status": True,
                "error": False,
                "data": geo
            }
            return Response(json_data, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({"error": str(e)},
                            status=status.HTTP_400_BAD_REQUEST)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
