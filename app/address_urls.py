from django.urls import path

from app import view_address, address_views

urlpatterns = [
    path('address/add/', view_address.INSERTAPIVIEW.as_view(), name='add'),
    path('address/get/', view_address.GETADDRESSAPIVIEW.as_view(),
         name='get_address_details'),
    path('address/get_id/<int:pk>', view_address.GETBYIDAPIVIEW.as_view(),
         name='get_address'),
    path('address/remove/<int:pk>',
         view_address.DELETEADDRESSAPIVIEW.as_view(), name='delete_address'),
    path('mongo/address/insert', address_views.AddressAPIVIEW.as_view(),
         name='mongo_address_insert'),

    path('mongo/address/get', address_views.GetAPIVIEW.as_view(),
         name='mongo_address_get'),
    path('mongo/address/get_id/<str:city>', address_views.GetAPIVIEWS.as_view(),
         name='mongo_address_get_id'),
    path('mongo/address/delete/<str:city>', address_views.DELETEAPIVIEW.as_view(), name='mongo_address_delete')
    ]

