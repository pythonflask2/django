import json

from bson import json_util
from rest_framework import status
from rest_framework.response import Response
from app.models import Person
from app.mongo_views import collection3
from app.serializer import Person_serializer
from rest_framework.generics import CreateAPIView, RetrieveAPIView, \
    DestroyAPIView, ListAPIView


class PersonAPIVIEW(CreateAPIView):
    serializer_class = Person_serializer
    queryset = Person.objects.all()

    def create(self, request, *args, **kwargs):
        person_name = request.data['person_name']
        dob = request.data['dob']
        age = request.data['age']
        employee_id = request.data['employee_id']
        document = {"age": age,
                    "person_name": person_name,
                    'dob': dob,
                    "employee_id": employee_id
                    }
        inserted_id = collection3.insert_one(document).inserted_id
        return Response({f'person details inserted'},
                        status=status.HTTP_201_CREATED)


class GetAPIVIEW(RetrieveAPIView):
    serializer_class = Person_serializer
    queryset = Person.objects.all()

    def get(self, request, *args, **kwargs):
        employee_details = collection3.find_one({"person_name": "string"})
        print(employee_details)
        return Response(json.loads(json_util.dumps(employee_details)))


class GetAPIVIEWS(ListAPIView):
    serializer_class = Person_serializer
    queryset = Person.objects.all()

    def get(self, request, *args, **kwargs):
        person_details = collection3.find()
        result = []
        for person in person_details:
            persons = {}
            persons['age'] = person.get('age')
            persons['dob'] = person.get('dob')
            persons['person_name'] = person.get('person_name')
            persons['employee_id'] = person.get('employee_id')
            result.append(persons)
            print(persons)
        return Response(json.loads(json_util.dumps(result)))


class DELETEAPIVIEW(DestroyAPIView):
    serializer_class = Person_serializer
    queryset = Person.objects.all()

    def delete(self, request, *args, **kwargs):
        person_name = kwargs.get('person_name')
        person_details = collection3.find_one_and_delete(
            {'person_name': person_name})
        return Response({f"person details deleted"})
