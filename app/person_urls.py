from django.urls import path

from app import views_person, person_views

urlpatterns = [
    path('person/info/', views_person.INSERTPERSONAPIVIEW.as_view(),
         name='person'),
    path('person/all_info/', views_person.GETALLAPIVIEW.as_view(),
         name='get_all_person_details'),
    path('person/delete/<int:pk>', views_person.DLELETEAPIVIEW.as_view(),
         name='delete_person'),
    path('person/get_by_id/<int:pk>',
         views_person.GETBYPERSONIDAPIVIEW.as_view(),
         name='get_person_details'),
    path('mongo/person/insert', person_views.PersonAPIVIEW.as_view(),
         name='mongo_person_insert'),
    path('mongo/person/get', person_views.GetAPIVIEWS.as_view(),
         name='mongo_person_get'),
    path('mongo/person/id/<str:person_name>', person_views.GetAPIVIEW.as_view(),
         name='mongo_person_get_id'),
    path('mongo/person/delete/<str:person_name>',
         person_views.DELETEAPIVIEW.as_view(), name='mongo_person_delete')
]
