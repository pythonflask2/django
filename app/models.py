import django
from django.contrib.auth.base_user import BaseUserManager
# Create your models here.
from django.db import models


class Employee(models.Model):
    id = models.AutoField(primary_key=True)
    employee_name = models.CharField(max_length=100)
    admin = models.BooleanField(default=None, null=True, blank=True)
    role = models.CharField(max_length=100)
    date_of_join = models.DateTimeField(default=None, null=True, blank=True)
    contact_number = models.CharField(max_length=100)
    is_active = models.BooleanField(default=None, null=True, blank=True)


class Address(models.Model):
    id = models.AutoField(primary_key=True)
    employee_id = models.ForeignKey(Employee, on_delete=models.CASCADE,
                                    null=True)
    place_of_birth = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    nationality = models.CharField(max_length=100)


class Person(models.Model):
    id = models.AutoField(primary_key=True)
    employee_id = models.ForeignKey(Employee, on_delete=models.CASCADE,
                                    null=True)
    person_name = models.CharField(max_length=100)
    age = models.IntegerField(default=None, null=True, blank=True)
    dob = models.DateTimeField(default=None, null=True, blank=True)


class Login(models.Model):
    id = models.AutoField(primary_key=True)
    user_name = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    admin = models.BooleanField(default=None, null=True, blank=True)
    is_active = models.BooleanField(default=True)


class Location(models.Model):
    location = models.CharField(max_length=200)
    destination = models.CharField(max_length=200)
    distance = models.DecimalField(max_digits=10, decimal_places=2)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Distance from {self.location} to {self.destination} is {self.distance} km"
