"""sample_django URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from channels.routing import URLRouter
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import include
from django.views.generic import TemplateView
from rest_framework_simplejwt import views as jwt_views
from django.urls import path

from app import views, view_location

# from app.views import test

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/',
         jwt_views.TokenObtainPairView.as_view(),
         name='token_obtain_pair'),
    path('api/token/refresh/',
         jwt_views.TokenRefreshView.as_view(), name='refresh_token'),
    path('app/', include('app.urls')),
    path('person/', include('app.person_urls')),
    path('address/', include('app.address_urls')),
    # path('web/', views.MessageSendAPIView.as_view(), name='websocket'),
    path('web/', views.index, name='websocket'),
    path('location/', view_location.geo_location, name='location')
    # path('app/insert/', views.PaginatedListView.as_view(), name='pagination')
]
