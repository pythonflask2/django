"""
ASGI config for sample_django project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/asgi/
"""

import os

from channels.auth import AuthMiddlewareStack
from channels.http import AsgiHandler
from channels.routing import ProtocolTypeRouter, URLRouter
import django

from django.urls import path

from app import views, routing

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'sample_django.settings')
django.setup()
application = ProtocolTypeRouter({
    "http": AsgiHandler(),
    "websocket": AuthMiddlewareStack(  # For Websocket Connection
        URLRouter(
            routing.websocket_urlpatterns
        )
    ),
})
